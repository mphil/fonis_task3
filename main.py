from random import randint


def main():
    rnum = str(randint(1000, 9999))
    print(rnum)
    inum = str(input("Enter number:"))
    print(inum)

    for current in range(0, 4):
        if rnum[current] == inum[current]:
            print(":D", end='|')
        elif rnum[current] != inum[current] and rnum[current] in [x for i, x in enumerate(inum) if i != current]:
            print(":)", end='|')
        else:
            print("X", end='|')


if __name__ == "__main__":
    main()
